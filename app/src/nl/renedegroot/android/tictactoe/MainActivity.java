/*------------------------------------------------------------------------------
 **     Ident: Smart Mobile
 **    Author: rene
 ** Copyright: (c) 23 nov. 2013 Sogeti Nederland B.V. All Rights Reserved.
 **------------------------------------------------------------------------------
 ** Sogeti Nederland B.V.            |  No part of this file may be reproduced  
 ** Solutions                        |  or transmitted in any form or by any        
 ** Lange Dreef 17                   |  means, electronic or mechanical, for the      
 ** 4131 NJ Vianen                   |  purpose, without the express written    
 ** The Netherlands                  |  permission of the copyright holder.
 *------------------------------------------------------------------------------
 */
package nl.renedegroot.android.tictactoe;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.google.android.gms.appstate.AppStateClient;
import com.google.android.gms.appstate.OnStateLoadedListener;
import com.google.example.games.basegameutils.BaseGameActivity;

public class MainActivity extends BaseGameActivity implements OnClickListener, AnimatorListener, OnStateLoadedListener
{
   private static final String PREFERENCE_SCORE = "scores";
   private static final String EXTRA_WON = "EXTRA_WON";
   private static final String EXTRA_LOST = "EXTRA_LOST";
   private static final String EXTRA_PLAYED = "EXTRA_PLAYED";
   private static final String EXTRA_PLAYERTURN = "EXTRA_PLAYERTURN";
   private static final int PLAYER_MARK = R.string.placeholder_X;
   private static final String EXTRA_PLAYER_POSITIONS = "EXTRA_PLAYER_POSITIONS";
   private static final String EXTRA_OPPONENT_POSITIONS = "EXTRA_OPPONENT_POSITIONS";
   private static final int OPPONENT_MARK = R.string.placeholder_O;
   private static final int GAME_WON = 0;
   private static final int GAME_LOST = 1;
   private static final int GAME_DRAW = 2;

   private TextView won;
   private TextView turn;
   private TextView played;

   private boolean isPlayerTurn;
   private TextView[] allFields;
   private ArrayList<Integer> playerPositions;
   private ArrayList<Integer> opponentPositions;
   private boolean isSignInFailed;

   public MainActivity()
   {
      // request AppStateClient and GamesClient
      super(BaseGameActivity.CLIENT_APPSTATE);
      enableDebugLog(true, "TicTacToe");
   }

   @Override
   protected void onCreate(Bundle savedInstanceState)
   {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      TextView topleft = (TextView) findViewById(R.id.field_topleft);
      TextView top = (TextView) findViewById(R.id.field_top);
      TextView topright = (TextView) findViewById(R.id.field_topright);
      TextView left = (TextView) findViewById(R.id.field_left);
      TextView center = (TextView) findViewById(R.id.field_center);
      TextView right = (TextView) findViewById(R.id.field_right);
      TextView bottomleft = (TextView) findViewById(R.id.field_bottomleft);
      TextView bottom = (TextView) findViewById(R.id.field_bottom);
      TextView bottomright = (TextView) findViewById(R.id.field_bottomright);

      allFields = new TextView[] { topleft, top, topright, left, center, right, bottomleft, bottom, bottomright };

      won = (TextView) findViewById(R.id.field_won);
      played = (TextView) findViewById(R.id.field_played);
      turn = (TextView) findViewById(R.id.field_turn);

      if (savedInstanceState == null)
      {
         isSignInFailed = false;
         isPlayerTurn = true;
         playerPositions = new ArrayList<Integer>(5);
         opponentPositions = new ArrayList<Integer>(5);
      }
      else
      {
         isSignInFailed = false;
         isPlayerTurn = savedInstanceState.getBoolean(EXTRA_PLAYERTURN);
         playerPositions = savedInstanceState.getIntegerArrayList(EXTRA_PLAYER_POSITIONS);
         opponentPositions = savedInstanceState.getIntegerArrayList(EXTRA_OPPONENT_POSITIONS);
      }

      setupBoardState();
   }

   private void setupBoardState()
   {
      setupHistory();
      for (int i = 0; i < allFields.length; i++)
      {
         allFields[i].setOnClickListener(this);
         if (playerPositions.contains(i) || opponentPositions.contains(i))
         {
            allFields[i].setAlpha(1.0F);
         }
         else
         {
            allFields[i].setAlpha(0.0F);
         }
      }
      makeTheNextTurn();
   }

   /**
    * TODO
    */
   private void setupHistory()
   {
      won.setText(Integer.toString(getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_WON, 0)));
      played.setText(Integer.toString(getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_PLAYED, 0)));
   }

   /**
    * Shows whos turn it is
    */
   private void makeTheNextTurn()
   {
      checkForWinner();
      if (isPlayerTurn)
      {
         turn.setVisibility(View.VISIBLE);
      }
      else
      {
         turn.setVisibility(View.INVISIBLE);
         ArrayList<Integer> options = new ArrayList<Integer>();
         for (int i = 0; i < 9; i++)
         {
            if (!playerPositions.contains(i) && !opponentPositions.contains(i))
            {
               options.add(i);
            }
         }
         int randomPosition = (int) (Math.random() * options.size());
         if (randomPosition < options.size())
         {
            int choice = options.get(randomPosition);
            isPlayerTurn = true;
            TextView field = allFields[choice];
            field.setText(OPPONENT_MARK);
            ObjectAnimator anim = ObjectAnimator.ofFloat(field, "alpha", field.getAlpha(), 1.0F);
            anim.setDuration(300);
            anim.start();
            anim.addListener(this);
            opponentPositions.add(choice);
         }
      }
   }

   private void checkForWinner()
   {
      boolean playerWon = false;
      boolean opponentWon = false;
      List<List<Integer>> winners = getWinners();
      for (Collection<Integer> winner : winners)
      {
         playerWon = playerPositions.containsAll(winner);
         opponentWon = opponentPositions.containsAll(winner);
         if (playerWon || opponentWon)
         {
            break;
         }
      }

      if (opponentWon || playerWon)
      {
         int won = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_WON, 0);
         int lost = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_LOST, 0);
         int played = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_PLAYED, 0);
         played++;
         if (playerWon)
         {
            won++;
         }
         if (opponentWon)
         {
            lost++;
         }
         saveStateLocal(won, lost, played);
         isPlayerTurn = true;
         playerPositions = new ArrayList<Integer>(5);
         opponentPositions = new ArrayList<Integer>(5);

         if (playerWon)
         {
            finishGame(GAME_WON);
         }
         if (opponentWon)
         {
            finishGame(GAME_LOST);
         }
      }
      else if (opponentPositions.size() + playerPositions.size() == 9)
      {
         Editor edit = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).edit();
         int played = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_PLAYED, 0);
         played++;
         edit.putInt(EXTRA_PLAYED, played);
         edit.apply();
         isPlayerTurn = true;
         playerPositions = new ArrayList<Integer>(5);
         opponentPositions = new ArrayList<Integer>(5);

         finishGame(GAME_DRAW);
      }

   }

   private void finishGame(int gameDraw)
   {
      saveState();
      final TextView banner = (TextView) findViewById(R.id.field_banner);
      switch (gameDraw)
      {
         case GAME_DRAW:
            banner.setText(R.string.game_draw);
            break;
         case GAME_LOST:
            banner.setText(R.string.game_lost);
            break;
         case GAME_WON:
            banner.setText(R.string.game_won);
            break;
         default:
            break;
      }

      banner.setVisibility(View.VISIBLE);
      AnimatorSet bouncer = new AnimatorSet();
      ObjectAnimator anim = ObjectAnimator.ofFloat(banner, "rotation", 0f, 360f, 0f, 360f);
      anim.setDuration(3000);
      bouncer.play(anim);
      anim = ObjectAnimator.ofFloat(banner, "scaleX", 1f, 2f, 1.5f, 3f);
      anim.setDuration(3000);
      bouncer.play(anim);
      anim = ObjectAnimator.ofFloat(banner, "scaleY", 1f, 2f, 1.5f, 3f);
      anim.setDuration(3000);
      bouncer.play(anim);
      bouncer.start();
      bouncer.addListener(new AnimatorListener()
         {

            @Override
            public void onAnimationStart(Animator animation)
            {
            }

            @Override
            public void onAnimationRepeat(Animator animation)
            {
            }

            @Override
            public void onAnimationEnd(Animator animation)
            {
               new Handler().postDelayed(new Runnable()
                  {

                     @Override
                     public void run()
                     {
                        banner.setVisibility(View.GONE);
                        setupBoardState();
                     }
                  }, 3000);

            }

            @Override
            public void onAnimationCancel(Animator animation)
            {
            }
         });

   }

   private List<List<Integer>> getWinners()
   {
      List<List<Integer>> winners = new ArrayList<List<Integer>>();
      winners.add(Arrays.asList(0, 1, 2));
      winners.add(Arrays.asList(3, 4, 5));
      winners.add(Arrays.asList(6, 7, 8));

      winners.add(Arrays.asList(0, 3, 6));
      winners.add(Arrays.asList(1, 4, 7));
      winners.add(Arrays.asList(2, 5, 8));

      winners.add(Arrays.asList(0, 4, 8));
      winners.add(Arrays.asList(2, 4, 6));

      return winners;
   }

   @Override
   protected void onSaveInstanceState(Bundle outState)
   {
      super.onSaveInstanceState(outState);
      outState.putBoolean(EXTRA_PLAYERTURN, isPlayerTurn);
      outState.putIntegerArrayList(EXTRA_PLAYER_POSITIONS, playerPositions);
      outState.putIntegerArrayList(EXTRA_OPPONENT_POSITIONS, opponentPositions);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu)
   {
      getMenuInflater().inflate(R.menu.main, menu);
      return true;
   }

   @Override
   public boolean onPrepareOptionsMenu(Menu menu)
   {
      menu.findItem(R.id.action_keepsynced).setVisible(isSignInFailed);
      return super.onPrepareOptionsMenu(menu);
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item)
   {
      boolean consumed = super.onOptionsItemSelected(item);
      if (!consumed)
      {
         switch (item.getItemId())
         {
            case R.id.action_keepsynced:
               beginUserInitiatedSignIn();
               consumed = true;
               break;
            default:
               break;
         }
      }
      return consumed;
   }

   @Override
   public void onClick(View v)
   {
      if (isPlayerTurn)
      {
         int position = viewToPosition(v);
         if (position >= 0 && !playerPositions.contains(position) && !opponentPositions.contains(position))
         {
            isPlayerTurn = false;
            allFields[position].setText(PLAYER_MARK);
            ObjectAnimator anim = ObjectAnimator.ofFloat(allFields[position], "alpha", allFields[position].getAlpha(),
                  1.0F);
            anim.setDuration(300);
            anim.start();
            anim.addListener(this);
            playerPositions.add(position);
         }
         else
         {
            Log.d(this, "Already taken field");
         }
      }
      else
      {
         Log.d(this, "Not the players turn right now");
      }
   }

   /**
    * From the ID of the fields determine the position on the board
    * 
    * @param field
    * @return
    */
   private Integer viewToPosition(View field)
   {
      int position = -1;
      int id = field.getId();
      for (int i = 0; i < allFields.length; i++)
      {
         if (allFields[i].getId() == id)
         {
            position = i;
            break;
         }
      }
      return position;
   }

   @Override
   public void onAnimationCancel(Animator animation)
   {
   }

   @Override
   public void onAnimationEnd(Animator animation)
   {
      makeTheNextTurn();
   }

   @Override
   public void onAnimationRepeat(Animator animation)
   {
   }

   @Override
   public void onAnimationStart(Animator animation)
   {
   }

   @Override
   public void onSignInFailed()
   {
      Log.e(this, "onSignInFailed()");
      isSignInFailed = true;
      invalidateOptionsMenu();
   }

   @Override
   public void onSignInSucceeded()
   {
      Log.e(this, "onSignInSucceeded()");
      getAppStateClient().loadState(this, 0);
      isSignInFailed = false;
      invalidateOptionsMenu();
   }

   @Override
   public void onStateConflict(int stateKey, String ver, byte[] localData, byte[] serverData)
   {
      Log.d(this, "onStateConflict(int stateKey, String ver, byte[] localData, byte[] serverData)");
   }

   @Override
   public void onStateLoaded(int statusCode, int stateKey, byte[] data)
   {

      if (statusCode == AppStateClient.STATUS_OK)
      {
         Log.d(this,
               "STATUS_OK: onStateLoaded(int " + statusCode + ", int " + stateKey + ", byte[] " + Arrays.toString(data)
                     + ")");
         ByteBuffer buffer = ByteBuffer.wrap(data);
         int won = buffer.getInt();
         int lost = buffer.getInt();
         int played = buffer.getInt();
         saveStateLocal(won, lost, played);
      }
      else if (statusCode == AppStateClient.STATUS_NETWORK_ERROR_STALE_DATA)
      {
         Log.d(this, "STATUS_NETWORK_ERROR_STALE_DATA: onStateLoaded(int " + statusCode + ", int " + stateKey
               + ", byte[] " + Arrays.toString(data) + ")");
      }
      else
      {
         Log.d(this,
               "ERROR OF SORT: onStateLoaded(int " + statusCode + ", int " + stateKey + ", byte[] "
                     + Arrays.toString(data) + ")");
      }
   }

   /**
    * @param won
    * @param lost
    * @param played
    */
   private void saveStateLocal(int won, int lost, int played)
   {
      Log.d(this, "Storing won " + won + ", lost " + lost + ",  played " + played + ".");
      Editor edit = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).edit();
      edit.putInt(EXTRA_WON, won);
      edit.putInt(EXTRA_LOST, lost);
      edit.putInt(EXTRA_PLAYED, played);
      edit.apply();
      setupHistory();
   }

   private void saveState()
   {
      int won = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_WON, 0);
      int lost = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_LOST, 0);
      int played = getSharedPreferences(PREFERENCE_SCORE, MODE_PRIVATE).getInt(EXTRA_PLAYED, 0);
      byte[] bytes = ByteBuffer.allocate(3 * 4).putInt(won).putInt(lost).putInt(played).array();

      if (getAppStateClient().isConnected())
      {
         Log.d(this, "getAppStateClient().updateState(" + 0 + ", " + Arrays.toString(bytes) + ")");
         getAppStateClient().updateState(0, bytes);
      }
   }
}
