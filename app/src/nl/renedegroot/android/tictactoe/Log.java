/*------------------------------------------------------------------------------
 **     Ident: Smart Mobile
 **    Author: rene
 ** Copyright: (c) 23 nov. 2013 Sogeti Nederland B.V. All Rights Reserved.
 **------------------------------------------------------------------------------
 ** Sogeti Nederland B.V.            |  No part of this file may be reproduced  
 ** Solutions                        |  or transmitted in any form or by any        
 ** Lange Dreef 17                   |  means, electronic or mechanical, for the      
 ** 4131 NJ Vianen                   |  purpose, without the express written    
 ** The Netherlands                  |  permission of the copyright holder.
 *------------------------------------------------------------------------------
 */
package nl.renedegroot.android.tictactoe;

/**
 * ????
 * 
 * @version $Id:$
 * @author rene (c) 23 nov. 2013, Sogeti B.V.
 */
public class Log
{
   private static final boolean DEBUG = true;

   /**
    * @param tag
    * @param message
    */
   public static void e(Object tag, String message)
   {
      if (DEBUG)
      {
         String logTag = "";
         if (tag instanceof String)
         {
            logTag = (String) tag;
         }
         else
         {
            logTag = tag.getClass().getSimpleName();
         }
         android.util.Log.e(logTag, message);
      }
   }

   /**
    * @param tag
    * @param message
    */
   public static void d(Object tag, String message)
   {
      if (DEBUG)
      {
         String logTag = "";
         if (tag instanceof String)
         {
            logTag = (String) tag;
         }
         else
         {
            logTag = tag.getClass().getSimpleName();
         }
         android.util.Log.d(logTag, message);
      }
   }
}
